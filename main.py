import keras
import numpy as np
from keras.layers import Dense
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.models import Sequential
from kerastuner.tuners import RandomSearch
from sklearn.metrics import confusion_matrix, classification_report

(x_train, y_train), (x_test, y_test) = mnist.load_data()

image_vector_size = 28 * 28
x_train = x_train.reshape(x_train.shape[0], image_vector_size)
x_test = x_test.reshape(x_test.shape[0], image_vector_size)

print("Training label shape: ", y_train.shape)

num_classes = 10
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

image_size = 784

def create_model(hyperparameters):
    model = Sequential()
    model.add(Dense(units=hyperparameters.Int('units_1', min_value=32, max_value=512, step=32),
                    activation='sigmoid', input_shape=(image_size,)))

    for i in range(hyperparameters.Int('num_layers', min_value=1, max_value=5)):
        model.add(Dense(units=hyperparameters.Int(f'units_{i + 2}', min_value=32, max_value=512, step=32),
                        activation='sigmoid'))

    model.add(Dense(units=num_classes, activation='softmax'))
    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
    return model


tuner = RandomSearch(
    create_model,
    objective='val_accuracy',
    max_trials=25,
    executions_per_trial=1,
    directory='tuner_dir',
    project_name='mnist_tuning'
)

tuner.search(x_train, y_train, epochs=30, validation_split=0.15)

best_model = tuner.get_best_models(num_models=1)[0]
best_hyperparameters = tuner.get_best_hyperparameters(num_trials=1)[0]

best_model.summary()

test_loss, test_accuracy = best_model.evaluate(x_test, y_test)
print("Test Loss:", test_loss)
print("Test Accuracy:", test_accuracy)

y_pred = best_model.predict(x_test)
y_pred_classes = y_pred.argmax(axis=1)
y_test_classes = y_test.argmax(axis=1)

cm = confusion_matrix(y_test_classes, y_pred_classes)
plt.figure(figsize=(8, 6))
plt.imshow(cm, cmap='Blues')
plt.title('Confusion Matrix')
plt.colorbar()
tick_marks = np.arange(num_classes)
plt.xticks(tick_marks, range(num_classes))
plt.yticks(tick_marks, range(num_classes))
plt.xlabel('Predicted Label')
plt.ylabel('True Label')
for i in range(num_classes):
    for j in range(num_classes):
        plt.text(j, i, cm[i, j], ha='center', va='center', color='white' if cm[i, j] > cm.max() / 2 else 'black')
plt.show()

report = classification_report(y_test_classes, y_pred_classes)
print("Classification Report:")
print(report)

best_model.save("best_model.h5")
print("Best model saved")
print("Best Hyperparameters:")
print(best_hyperparameters.values)
